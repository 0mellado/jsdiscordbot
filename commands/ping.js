const {Client, Intents} = require('discord.js')
const {SlashCommandBuilder} = require('@discordjs/builders');

const client = new Client({intents: [Intents.FLAGS.GUILDS]});

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ping')
        .setDescription('Replies with Pong!'),
    async execute(interaction) {
        const sent = await interaction.reply({content: 'pingeando...', fetchReply: true});
        const ping = sent.createdTimestamp - interaction.createdTimestamp;
        interaction.editReply(`pong con: ${ping}ms`);
    },
};

