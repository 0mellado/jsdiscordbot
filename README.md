# Bot de discord que ejecuta código

## Instalación:

### Descargar el repositorio:

```
git clone https://gitlab.com/0mellado/jsdiscordbot.git
```

### Compilar la imagen de docker:

```
sudo docker build -t bot:1.0 .
```

### Ejecutar la imagen de docker:
```
sudo docker run -p 3000:3000 -d bot:1.0
```

## Forma para modificar el código:

Si se desea modificar el código es recomendable ejecutar el archivo index.js de la siguiente forma:
```
npm run dev
```
de esa forma cada vez que se guarde algún archivo dentro del directorio se va a reiniciar automáticamente el bot.
