const fs = require('node:fs');
const path = require('node:path');
const {exec} = require('child_process');

module.exports = {
    name: 'run',
    async execute(message) {
        const request = message.content;
        let code = request
            .replace(/-run|```/g, "")
            .trim().split("\n");
        const lang = code.shift().replace(/\n/g, "");
        const langsPath = path.join(__dirname, 'langs');
        const langsFiles = fs.readdirSync(langsPath)
            .filter(file => file.endsWith('.js'));

        for (const file of langsFiles) {
            const filePath = path.join(langsPath, file);
            const langCode = require(filePath);

            if (lang == langCode.name) {
                langCode.execute(code);
            }
        }

        await new Promise(resolve => setTimeout(resolve, 900));
        fs.open('execLangs/response', 'r', (err, file) => {
            if (err) throw err;
            fs.readFile(file, 'utf8', (err, data) => {
                if (err) throw err;
                message.reply(`\`\`\`${data.toString()}\`\`\``);
            });
        });

        exec('rm -rf execLangs/response', (err, stdout, stderr) => {
            if (err) return;
            if (stderr) return;
        });
    }
}
