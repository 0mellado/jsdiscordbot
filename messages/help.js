const {MessageEmbed} = require('discord.js');

const random = (min, max) => {
    return Math.floor((Math.random() * (max - min + 1)) + min);
}

const randColor = () => {
    r = random(1, 255);
    g = random(1, 255);
    b = random(1, 255);
    return [r, g, b];
};


module.exports = {
    name: 'help',
    async execute(message, args) {
        if (args.length) {
            if (args[0] == 'run') {
                const embedRun = new MessageEmbed()
                    .setTitle('Comando -run')
                    .setColor(randColor())
                    .setDescription('Este comando ejecuta el código que le mandes')
                    .setImage('https://pillan.inf.uct.cl/~omellado/actividades/image/help1.png')
                message.channel.send({embeds: [embedRun]});
                return;
            }
        }

        const embedHelp = new MessageEmbed()
            .setTitle('Comando de Ayuda')
            .setColor(randColor())
            .setDescription('Comandos:')
            .addFields(
                {
                    name: '-run',
                    value: 'Este comando ejecuta el código que le mandes'
                },
                {
                    name: '-help',
                    value: 'Este comando muestra este mismo output'
                }
            );
        await message.channel.send({embeds: [embedHelp]});
    }
};

