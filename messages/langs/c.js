const fs = require('node:fs');
const {exec} = require('child_process');

module.exports = {
    name: 'c',
    async execute(code) {
        fs.open('execLangs/code.c', 'w', (err, file) => {
            if (err) throw err;
            for (let line of code) {
                fs.writeFile(file, `${line}\n`, err => {
                    if (err) throw err;
                });
            }
        });

        exec('gcc execLangs/code.c -o execLangs/a.out', (err, stdout, stderr) => {
            if (err) return;
            if (stderr) return;
        });
        await new Promise(resolve => setTimeout(resolve, 500));
        exec('./execLangs/a.out > execLangs/response', (error, stdout, stderr) => {
            if (error) return;
            if (stderr) return;
        });
    }
}
