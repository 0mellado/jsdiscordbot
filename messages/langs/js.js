const fs = require('node:fs');
const {exec} = require('child_process');

module.exports = {
    name: 'js',
    execute(code) {
        fs.open('execLangs/code', 'w', (err, file) => {
            if (err) throw err;
            for (let line of code) {
                fs.writeFile(file, `${line}\n`, err => {
                    if (err) throw err;
                });
            }
        });

        exec('node execLangs/code > execLangs/response', (error, stdout, stderr) => {
            if (error) return;
            if (stderr) return;
        });
    }
}
