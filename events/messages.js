const fs = require('node:fs');
const path = require('node:path');
const {Client, Intents, Collection} = require('discord.js');

const prefix = "-";
const client = new Client({intents: [Intents.FLAGS.GUILDS]});

module.exports = {
    name: 'messageCreate',
    async execute(message) {

        const commandsPath = path.join(__dirname, '../messages');
        const commandFiles = fs.readdirSync(commandsPath)
            .filter(file => file.endsWith('.js'));

        client.commands = new Collection();

        for (const file of commandFiles) {
            const filePath = path.join(commandsPath, file);
            const command = require(filePath);

            client.commands.set(command.name, command);
        }

        if (!message.author.bot) {

            const args = message.content.slice(prefix.length).trim().split(/ +/g);
            const precmd = args.shift().replace('\n', ' ').split(' ').shift()
            const command = client.commands.get(precmd);

            try {
                await command.execute(message, args);
            } catch (err) {
                console.error(err);
            }
        }
    },
};

