const fs = require('node:fs');
const path = require('node:path');
const {Client, Collection, Intents} = require('discord.js');

const client = new Client({intents: [Intents.FLAGS.GUILDS]});

module.exports = {
    name: 'interactionCreate',
    async execute(interaction) {

        const commandsPath = path.join(__dirname, '../commands/');
        const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

        client.commands = new Collection();

        for (const file of commandFiles) {
            const filePath = path.join(commandsPath, file);
            const command = require(filePath);

            client.commands.set(command.data.name, command);
        }

        if (!interaction.isCommand()) return;

        const command = client.commands.get(interaction.commandName);

        if (!command) return;

        try {
            await command.execute(interaction);
        } catch (error) {
            console.error(error);
            await interaction.reply({content: 'There was an error while executing this command!', ephemeral: true});
        }
    },
};
